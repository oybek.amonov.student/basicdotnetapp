# Before adding the .NET app to the Docker image, first it must be published.
# It is best to have the container run the published version of the app. To publish the app, run the following command:
dotnet publish -c Release
# Docker will process each line in the Dockerfile. The . in the docker build command sets the build context of the image.
docker build -t tosquare-image -f Dockerfile .
# Docker provides the docker run command to create and run the container as a single command.
# This command eliminates the need to run docker create and then docker start.
# You can also set this command to automatically delete the container when the container stops.
docker run -it --rm tosquare-image

# Expected Output
# Square of 5 = 25