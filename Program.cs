﻿class Program{
    static void Main(string[] args){
        int value = 5;
        Console.WriteLine("Square of {0} = {1}", value, MathCustom.ToSquare(value));
    }
}
public class MathCustom{
    static public int ToSquare(int x){
            return x*x;
    }
}